<!DOCTYPE html>
<html>
<body>
    <input type="text" name="searchText" class="searchText" id="searchText" value="<?=$searchText?>" placeholder="Search...">
    <input type="submit" class="searchSubmit" value="Submit data" name="submit">
    <div><ul class="list"></ul></div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(".searchSubmit").click(_resultHandler);
$('.searchText').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        _resultHandler();
    }
});
function _resultHandler() {
  $.ajax({
    url: "/search_result.php",
    data: {
	pattern: $(".searchText").val()
    },
    success: function(result) {
      $('.list li').remove();
      $.each(JSON.parse(result), function(index, value) {
        $('.list').append("<li><img src='" + value["image"] + "'/></li>")
      });
    }
  });

}
</script>
</html>
